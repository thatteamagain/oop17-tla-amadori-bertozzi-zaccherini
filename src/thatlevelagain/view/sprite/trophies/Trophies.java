package thatlevelagain.view.sprite.trophies;

import java.awt.image.BufferedImage;

import thatlevelagain.view.sprite.SpriteImpl;

/**
 * 
 * general trophies class.
 *
 */
public class Trophies extends SpriteImpl {

   /**
     * constructor.
     * @param x
     *         x position
     * @param y
     *         y position
     * @param width
     *         shape's width
     * @param height
     *         shape's height
     * @param image
     *         image 
     */
    public Trophies(final int x, final int y, final int width, final int height, final BufferedImage image) {
        super(x, y, width, height, image);
    }

}
