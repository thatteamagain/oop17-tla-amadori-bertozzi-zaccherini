package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level8's hint.
 *
 */
public class Hint8 extends HintImpl {

    private static final String MESSAGE = "LIGHT'S OFF, TURN IT ON!";
    private static final String NAME = "GO TO OPTIONS MENU AND PRESS \"LIGHT ON\" BUTTON. DON'T GET COUGHT";

    /**
     * 
     */
    public Hint8() {
        super(NAME, MESSAGE);
    }

}
