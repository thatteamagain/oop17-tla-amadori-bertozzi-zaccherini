package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level5's hint.
 *
 */
public class Hint5 extends HintImpl {

    private static final String NAME = "DON'T TOUCH THE WOOL BALL! PRESS THE BUTTON TO OPEN THE DOOR";
    private static final String MESSAGE = "IT IS CUNNING..";
    /**
     * 
     */
    public Hint5() {
        super(NAME, MESSAGE);
    }

}
