package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level6's hint.
 *
 */
public class Hint6 extends HintImpl {

    private static final String NAME = "GO TO THE MENU. PRESS \"SKIP LEVEL\"";
    private static final String MESSAGE = "DEJA VU? IT LOOKS LIKE LEVEL THREE...";
    /**
     * 
     */
    public Hint6() {
        super(NAME, MESSAGE);
    }

}
