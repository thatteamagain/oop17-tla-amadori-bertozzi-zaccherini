package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level9's hint.
 *
 */
public class Hint9 extends HintImpl {

    private static final String MESSAGE = "SEVEREAL TIMES...";
    private static final String NAME = "PRESS THE BUTTON FOUR TIMES";

    /**
     * 
     */
    public Hint9() {
        super(NAME, MESSAGE);
    }

}
