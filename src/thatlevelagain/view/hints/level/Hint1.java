package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level1's hint.
 *
 */
public class Hint1 extends HintImpl {

    private static final String MESSAGE = "PRESS...";
    private static final String NAME = "JUST PRESS THE BUTTON";

    /**
     * 
     */
    public Hint1() {
        super(NAME, MESSAGE);
    }

}
