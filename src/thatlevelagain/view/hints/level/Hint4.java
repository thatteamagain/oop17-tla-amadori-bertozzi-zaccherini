package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level4's hint.
 *
 */
public class Hint4 extends HintImpl {

    private static final String MESSAGE = "DON'T TRUST YOUR EYES";
    private static final String NAME = "GO TROUGHT THE DOOR EVEN IF IT SEEMS CLOSED. DON'T PRESS THE BUTTON";

    /**
     * 
     */
    public Hint4() {
        super(NAME, MESSAGE);
    }

}
