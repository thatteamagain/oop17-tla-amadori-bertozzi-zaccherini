package thatlevelagain.view.hints.level;

import thatlevelagain.view.hints.HintImpl;

/**
 * 
 * level3's hint.
 *
 */
public class Hint3 extends HintImpl {

    private static final String MESSAGE = "DO YOU LIKE OPTIONS MENU?";
    private static final String NAME = "PRESS \"OPEN DOOR\" IN THE OPTIONS MENU TO OPEN THE DOOR";
    /**
     * 
     */
    public Hint3() {
        super(NAME, MESSAGE);
    }

}
